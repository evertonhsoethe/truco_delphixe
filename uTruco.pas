unit uTruco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ImgList, Math, ExtCtrls, ActnList,
  JvExStdCtrls, JvButton, JvCtrls, JvImageList, MMSystem, uAbout;

type
  TfoTruco = class(TForm)
    OURO: TImageList;
    Timer1: TTimer;
    COPAS: TImageList;
    ESPADA: TImageList;
    PAUS: TImageList;
    ImagemDefault: TImageList;
    Panel2: TPanel;
    VezIA: TPanel;
    CartaJogadaIA: TJvImgBtn;
    CartaVirada: TJvImgBtn;
    CartaJogada: TJvImgBtn;
    VezPlayer: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edEles: TEdit;
    edNos: TEdit;
    edI2: TEdit;
    edI1: TEdit;
    edI3: TEdit;
    edJ2: TEdit;
    edJ1: TEdit;
    edJ3: TEdit;
    BtReset: TBitBtn;
    edC1: TEdit;
    BtDesenvolvedor: TBitBtn;
    BtTruco: TBitBtn;
    Panel1: TPanel;
    btTrucoIA: TBitBtn;
    Carta1: TJvImgBtn;
    Carta2: TJvImgBtn;
    Carta3: TJvImgBtn;
    CartaIA1: TJvImgBtn;
    CartaIA2: TJvImgBtn;
    CartaIA3: TJvImgBtn;
    btPadrao: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    cbMensagens: TCheckBox;
    cbSons: TCheckBox;
    procedure formShow(Sender: TObject);
    procedure Carta3Click(Sender: TObject);
    procedure Carta2Click(Sender: TObject);
    procedure Carta1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtTrucoClick(Sender: TObject);
    procedure btTrucoIAClick(Sender: TObject);
    procedure CartaViradaClick(Sender: TObject);
    procedure BtResetClick(Sender: TObject);
    procedure BtDesenvolvedorClick(Sender: TObject);
    procedure btPadraoClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
     Baralho : Array [1..4,1..10] of String;
     Baralho2 : Array [1..4,1..10] of String;
     Jogo : Array[1..7] of String;
     Player1 : Array[1..3] of String;
     IA : Array[1..3] of String;
     PesoCartasIA : Array [1..3,1..2] of Integer;
     Coringa : String;
     FgComeca : Integer;
     FgComecaRodada : Integer;
     Rodada : Integer;
     QtRodadasGanhaIA : Integer;
     QtRodadasGanhaPlayer : Integer;
     PontosValidosJogo : Integer;
     ForcaMaoIA : Integer;
     bEmpachou, FgJogoIniciado,
     FgTrucoIA, FgTrucoPlayer,
     FgEmpachou1, FgEmpachou2,
     FgModoDesenvolvedor,
     FgAceitaTrucoIA : Boolean;
     FgFacao, FgTrucoRodada,
     FgRetrucoRodada : Boolean;
     DiretorioSound : PWideChar;
     InFezPrimeira : Integer;
     procedure DarCartas;
     procedure ValidaJogada;
     procedure VerificaQuemComeca;
     procedure IniciaJogo;
     procedure PlayerRecebePontos(Pontos : Integer);
     procedure IARecebePontos(Pontos : Integer);
     procedure PlayerVenceRodada;
     procedure IAVenceRodada;
     function ClassificaMaoIA(Mao : Array Of String) : Integer;
     procedure HabilitaDesabilitaTrucar;
     function  ProcuraCartaNaMaoMaisForteIA(CartaPlayer : String = '') : Integer;
     function  validaJogo(Carta : String) : Boolean;
     Function  PerguntaSN(Const Msg : String;
                          DefButton : Word) : Word;
     Function EncontraCartaMaisFraca(Vetor : Array of String) : Integer;
     procedure TiraCartaMao(CartaIA : Integer);
     procedure DefinirBaralho;
     procedure SetaImagemCarta(Sender:TObject;Caption : String);
     procedure InicializaImagemCartas;
     procedure ReiniciaJogo;
     procedure LimpaPesoCartasIA;
     procedure AtribuirPesoCartasIA(Mao : Array Of String);
     function EncontraCartaMedia(Mao : Array Of String) : Integer;
     function EncontraCartaForte(Mao : Array Of String) : Integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  foTruco: TfoTruco;

implementation

{$R *.dfm}



{ TForm1 }

procedure TfoTruco.AtribuirPesoCartasIA(Mao : Array Of String);
var i, j : Integer;
begin
   for I := 1 to 4 do
      for j := 1 to 10 do
      begin
         if (Baralho[i,j] = Mao[0]) then
         begin
            PesoCartasIA[1,1] := i;
            PesoCartasIA[1,2] := j;
         end;
         if (Baralho[i,j] = Mao[1]) then
         begin
            PesoCartasIA[2,1] := i;
            PesoCartasIA[2,2] := j;
         end;
         if (Baralho[i,j] = Mao[2]) then
         begin
            PesoCartasIA[3,1] := i;
            PesoCartasIA[3,2] := j;
         end;
      end;
end;

procedure TfoTruco.btPadraoClick(Sender: TObject);
begin
   edC1.Text := '4-P';

   //Seta M�o Player
   edJ1.text := '1-O';
   edJ2.text := '2-O';
   edJ3.text := '3-O';

   //Seta M�o IA
   edI1.text := '1-P';
   edI2.text := '2-P';
   edI3.text := '3-P';
end;

procedure TfoTruco.BitBtn1Click(Sender: TObject);
begin
   FgComecaRodada := 0;
end;

procedure TfoTruco.BitBtn2Click(Sender: TObject);
begin
   foAbout := TfoAbout.Create(nil);
   foAbout.ShowModal;
end;

procedure TfoTruco.BtDesenvolvedorClick(Sender: TObject);
begin
   FgModoDesenvolvedor := True;
   SetaImagemCarta(CartaIA1,CartaIA1.Caption);
   SetaImagemCarta(CartaIA2,CartaIA2.Caption);
   SetaImagemCarta(CartaIA3,CartaIA3.Caption);
end;

procedure TfoTruco.BtResetClick(Sender: TObject);
begin
   ReiniciaJogo;
   Timer1.Enabled := True;
   Coringa := edC1.Text;
   PontosValidosJogo := 1;
   fgEmpachou1 := False;
   fgEmpachou2 := False;
   bEmpachou := False;
   Player1[1] := edJ1.text;
   Player1[2] := edJ2.text;
   Player1[3] := edJ3.text;
   InFezPrimeira := 3;
   //Seta M�o IA
   IA[1] := edI1.text;
   IA[2] := edI2.text;
   IA[3] := edI3.text;

   Jogo[1] := Player1[1];
   Jogo[2] := Player1[2];
   Jogo[3] := Player1[3];
   Jogo[4] := IA[1];
   Jogo[5] := IA[2];
   Jogo[6] := IA[3];
   Jogo[7] := Coringa;
   //Atribui graficamente a carta
   Carta1.Caption := Player1[1];
   Carta2.Caption := Player1[2];
   Carta3.Caption := Player1[3];
   CartaIA1.Caption := IA[1];
   CartaIA2.Caption := IA[2];
   CartaIA3.Caption := IA[3];

   SetaImagemCarta(Carta1,Player1[1]);
   SetaImagemCarta(Carta2,Player1[2]);
   SetaImagemCarta(Carta3,Player1[3]);
   SetaImagemCarta(CartaIA1,IA[1]);
   SetaImagemCarta(CartaIA2,IA[2]);
   SetaImagemCarta(CartaIA3,IA[3]);

   Carta1.Visible := Enabled;
   Carta2.Visible := Enabled;
   Carta3.Visible := Enabled;
   CartaIA1.Visible := Enabled;
   CartaIA2.Visible := Enabled;
   CartaIA3.Visible := Enabled;

   ForcaMaoIA := ClassificaMaoIA(IA);

   if ForcaMaoIA >= 10 then
      FgAceitaTrucoIA := True;

   VerificaQuemComeca;

   CartaVirada.Caption := Coringa;
   SetaImagemCarta(CartaVirada,Coringa);
end;

procedure TfoTruco.BtTrucoClick(Sender: TObject);
var MsgTruco : Integer;
begin
   Timer1.Enabled := False;
   if PontosValidosJogo = 1 then
      PontosValidosJogo := 3
   else
   if (FgTrucoIA) //Se a IA trucou e o Player est� retrucando
   and (PontosValidosJogo >= 3)
   and (PontosValidosJogo <= 9)then //N�o pode entrar se a pontua��o apostada for maior que 9
   begin
      PontosValidosJogo := PontosValidosJogo + 3;
   end;

   if FgAceitaTrucoIA then
   begin
      FgTrucoPlayer := True;

      if not (FgTrucoIA) //N�o foi o player que trucou primeiro
      and (PontosValidosJogo > 3)
      and (PontosValidosJogo <= 9) then
      begin
         PontosValidosJogo := PontosValidosJogo + 3;
         if PontosValidosJogo = 12 then
            BtTruco.Enabled := False;
      end;
      ShowMessage('IA Aceitou o truco valendo '+IntTOStr(PontosValidosJogo)+' pontos');

      FgTrucoIA := False;
   end else
   begin
      PlaySound('C:\Efeitos Sonoros\E24.WAV', 1, SND_ASYNC);

      if not cbMensagens.Checked then
         MsgTruco := 0
      else
         MsgTruco := RandomRange(1,6);

      case MsgTruco of
         0 : ShowMessage('IA correu!');
         1 : ShowMessage('Voc� realmente sabe passar fac�o, n�o cairei no proximo!!');
         2 : ShowMessage('Tr�s 4 ningu�m merece!!');
         3 : ShowMessage('Voc� est� roubando, pare de utilizar o modo desenvovedor e me enfrete como homem!!');
         4 : ShowMessage('Com esta m�o poderia come�ar uma Canastra!');
         5 : ShowMessage('Vou lhe dar esta chance para se recuperar :D');
      end;

      FgTrucoPlayer := False;
      if PontosValidosJogo > 3 then
         PlayerRecebePontos(PontosValidosJogo-3)
      else
         PlayerRecebePontos(PontosValidosJogo-2);
   end;
   HabilitaDesabilitaTrucar;
   Timer1.Enabled := True;
end;

procedure TfoTruco.btTrucoIAClick(Sender: TObject);
var MsgTruco : Integer;
begin
   Timer1.Enabled := False;

   PlaySound('C:\Efeitos Sonoros\C23.WAV', 1, SND_ASYNC);

   if PontosValidosJogo = 1 then
      PontosValidosJogo := 3
   else
   if (FgTrucoPlayer) //Se o player trucou e a IA est� retrucando
   and (PontosValidosJogo >= 3)
   and (PontosValidosJogo <= 9)then //N�o pode entrar se a pontua��o apostada for maior que 9
   begin
      PontosValidosJogo := PontosValidosJogo + 3;
   end;

   if PerguntaSN('IA pede '+IntToStr(PontosValidosJogo)+' pontos voc� aceita ou corre?',mrYes) = mrYes then
   begin
      FgTrucoIA := True;

      if PontosValidosJogo = 1 then
         PontosValidosJogo := 3;

      if not (FgTrucoPlayer) //N�o foi o player que trucou primeiro
      and (PontosValidosJogo > 3)
      and (PontosValidosJogo <= 9) then
      begin
         PontosValidosJogo := PontosValidosJogo + 3;
         if PontosValidosJogo = 12 then
            BtTruco.Enabled := False;
      end;
      FgTrucoPlayer := False;
   end else
   begin
      PlaySound('C:\Efeitos Sonoros\E24.WAV', 1, SND_ASYNC);
      if not cbMensagens.Checked then
         MsgTruco := 0
      else
         MsgTruco := RandomRange(1,5);

      case MsgTruco of
         0 : ShowMessage('Escapou desta vez!');
         1 : ShowMessage('Amarelou!!');
         2 : ShowMessage('Corre mesmo, lugar de mulherzinha � no baralho!!');
         3 : ShowMessage('Justo quando estava com o gato!!');
         4 : ShowMessage('Sorte sua correr, minha m�o estava co�ando para colar o gato na sua testa!');
      end;

      FgTrucoIA := False;
      if PontosValidosJogo > 3 then
         IARecebePontos(PontosValidosJogo-3)
      else
         IARecebePontos(PontosValidosJogo-2);
   end;
   HabilitaDesabilitaTrucar;
   Timer1.Enabled := True;
end;

procedure TfoTruco.Carta1Click(Sender: TObject);
var i : integer;
begin
   CartaJogada.Visible := True;
   CartaJogada.Caption := Carta1.Caption;
   SetaImagemCarta(CartaJogada,Carta1.Caption);

   for I := 1 to 6 do
   begin
      if Jogo[i] = Carta1.Caption then
      begin
         Jogo[i] := '';
         Player1[1] := '';
         Carta1.Visible := False;
      end;
   end;
   VezPlayer.Visible := False;
   VezIA.Visible := True;
end;

procedure TfoTruco.Carta2Click(Sender: TObject);
var i : integer;
begin
   CartaJogada.Visible := True;
   CartaJogada.Caption := Carta2.Caption;
   SetaImagemCarta(CartaJogada,Carta2.Caption);

   for I := 1 to 6 do
   begin
      if Jogo[i] = Carta2.Caption then
      begin
         Jogo[i] := '';
         Player1[2] := '';
         Carta2.Visible := False;
      end;
   end;
   VezPlayer.Visible := False;
   VezIA.Visible := True;
   HabilitaDesabilitaTrucar;
end;

procedure TfoTruco.Carta3Click(Sender: TObject);
var i : integer;
begin
   CartaJogada.Visible := True;
   CartaJogada.Caption := Carta3.Caption;
   SetaImagemCarta(CartaJogada,Carta3.Caption);

   for I := 1 to 6 do
   begin
      if Jogo[i] = Carta3.Caption then
      begin
         Jogo[i] := '';
         Player1[3] := '';
         Carta3.Visible := False;
      end;
   end;
   VezPlayer.Visible := False;
   VezIA.Visible := True;
   HabilitaDesabilitaTrucar;
end;

procedure TfoTruco.CartaViradaClick(Sender: TObject);
begin
   if not FgJogoIniciado then
   begin
      FgJogoIniciado := true;
      IniciaJogo;
   end;
end;

function TfoTruco.ClassificaMaoIA(Mao : Array Of String) : Integer;
begin
   Result := 0;
   {
      4 a 12 - fraco
      4 a 12 e duas cartas maior igual a 2 - forte  = >10

      Caso a fun��o retorne
   }
   LimpaPesoCartasIA;
   AtribuirPesoCartasIA(Mao); //Seta o Array com os pesos das cartas da ia onde a primeira coluna � Naipe e a segunda � Carta.

   //Valida��es de classifica��o

   if (PesoCartasIA[1,1] in [1,2,3,4])        //Compara linha ou Naipe
   and (PesoCartasIA[1,2] in [1,2]) then  //Compara coluna ou Numero
      Result := Result+5
   else
      Result := Result;

   if (PesoCartasIA[2,1] in [1,2,3,4])        //Compara linha ou Naipe
   and (PesoCartasIA[2,2] in [1,2]) then  //Compara coluna ou Numero
      Result := Result+5
   else
      Result := Result;

   if (PesoCartasIA[3,1] in [1,2,3,4])        //Compara linha ou Naipe
   and (PesoCartasIA[3,2] in [1,2]) then  //Compara coluna ou Numero
      Result := Result+5
   else
      Result := Result;
end;

procedure TfoTruco.DarCartas;
var i, j, x : integer;
    Carta : String;
begin
   Carta := '';
   Coringa := Baralho[RandomRange(1,5),RandomRange(1,11)];
   Jogo[7] := Coringa;

   //Seta M�o Player
   for I := 1 to 3 do
   begin
      Carta := Baralho[RandomRange(1,5),RandomRange(1,11)];
         while validaJogo(Carta) do
            Carta := Baralho[RandomRange(1,5),RandomRange(1,11)];

      Player1[i] := Carta;
      Jogo[i] := Player1[i];
   end;

   //Seta M�o IA
   for x := 1 to 3 do
   begin
      Carta := Baralho[RandomRange(1,5),RandomRange(1,11)];
         while validaJogo(Carta) do
            Carta := Baralho[RandomRange(1,5),RandomRange(1,11)];

      IA[x] := Carta;
      Jogo[i] := IA[x];
      Inc(i);
   end;

   //Atribui graficamente a carta
   Carta1.Caption := Player1[1];
   Carta2.Caption := Player1[2];
   Carta3.Caption := Player1[3];
   CartaIA1.Caption := IA[1];
   CartaIA2.Caption := IA[2];
   CartaIA3.Caption := IA[3];

   SetaImagemCarta(Carta1,Player1[1]);
   SetaImagemCarta(Carta2,Player1[2]);
   SetaImagemCarta(Carta3,Player1[3]);
   SetaImagemCarta(CartaIA1,IA[1]);
   SetaImagemCarta(CartaIA2,IA[2]);
   SetaImagemCarta(CartaIA3,IA[3]);

   Carta1.Visible := Enabled;
   Carta2.Visible := Enabled;
   Carta3.Visible := Enabled;
   CartaIA1.Visible := Enabled;
   CartaIA2.Visible := Enabled;
   CartaIA3.Visible := Enabled;

   CartaVirada.Caption := Coringa;
   SetaImagemCarta(CartaVirada,Coringa);
   //showmessage(coringa);
   //CartaVirada.Enabled := False;
end;




procedure TfoTruco.DefinirBaralho;
begin
   //Define as cartas e posi��es de acordo com uma matriz:
   //Linha  (i) = Naipe. Exemplo: C - Copas, O - Ouro
   //Coluna (j) = Carta. Exemplo: 4 at� 12 e 1 at� 3 (for�a da carta quando n�o coringa)

   Baralho[1,1]  := '3-P';
   Baralho[1,2]  := '2-P';
   Baralho[1,3]  := '1-P';
   Baralho[1,4]  := '12-P';
   Baralho[1,5]  := '11-P';
   Baralho[1,6]  := '10-P';
   Baralho[1,7]  := '7-P';
   Baralho[1,8]  := '6-P';
   Baralho[1,9]  := '5-P';
   Baralho[1,10] := '4-P';

   Baralho[2,1]  := '3-C';
   Baralho[2,2]  := '2-C';
   Baralho[2,3]  := '1-C';
   Baralho[2,4]  := '12-C';
   Baralho[2,5]  := '11-C';
   Baralho[2,6]  := '10-C';
   Baralho[2,7]  := '7-C';
   Baralho[2,8]  := '6-C';
   Baralho[2,9]  := '5-C';
   Baralho[2,10] := '4-C';

   Baralho[3,1]  := '3-E';
   Baralho[3,2]  := '2-E';
   Baralho[3,3]  := '1-E';
   Baralho[3,4]  := '12-E';
   Baralho[3,5]  := '11-E';
   Baralho[3,6]  := '10-E';
   Baralho[3,7]  := '7-E';
   Baralho[3,8]  := '6-E';
   Baralho[3,9]  := '5-E';
   Baralho[3,10] := '4-E';

   Baralho[4,1]  := '3-O';
   Baralho[4,2]  := '2-O';
   Baralho[4,3]  := '1-O';
   Baralho[4,4]  := '12-O';
   Baralho[4,5]  := '11-O';
   Baralho[4,6]  := '10-O';
   Baralho[4,7]  := '7-O';
   Baralho[4,8]  := '6-O';
   Baralho[4,9]  := '5-O';
   Baralho[4,10] := '4-O';

   //Inicializa Placar (0 � 0).
   Coringa     := '';
   edEles.Text := '0';
   edNos.Text  := '0';

   InicializaImagemCartas;
end;

function TfoTruco.EncontraCartaForte(Mao: array of String): Integer;
var i,j,
    Linha1, Coluna1, //Carta 1
    Linha2, Coluna2, //Carta 2
    Linha3, Coluna3, //Carta 3
    CoringaColuna : integer;
begin
   Linha1:=0;
   Coluna1:=0;
   Linha2:=0;
   Coluna2:=0;
   Linha3:=0;
   Coluna3:=0;

   for I := 1 to 4 do
      for j := 1 to 10 do
      begin
         if Mao[0] <> '' then
            if (Baralho[i,j] = Mao[0]) then
            begin
               Linha1 := i;
               Coluna1 := j;
            end;
         if Mao[1] <> '' then
            if (Baralho[i,j] = Mao[1]) then
            begin
               Linha2 := i;
               Coluna2 := j;
            end;
         if Mao[2] <> '' then
            if (Baralho[i,j] = Mao[2]) then
            begin
               Linha3 := i;
               Coluna3 := j;
            end;
         if (Baralho[i,j] = Coringa) then
         begin
            CoringaColuna := j-1;
         end;
      end;

   if CoringaColuna = 1 then
      CoringaColuna := 10
   else
      Dec(CoringaColuna);

   if CoringaColuna = 1 then
   begin
      if Coluna1 = 0 then
         Coluna1 := 9;
      if Coluna2 = 0 then
         Coluna2 := 9;
      if Coluna3 = 0 then
         Coluna3 := 9;
   end else
   begin
      if Coluna1 = 0 then
         Coluna1 := 10;
      if Coluna2 = 0 then
         Coluna2 := 10;
      if Coluna3 = 0 then
         Coluna3 := 10;
   end;

   if (Coluna1 <> CoringaColuna)
   and (Coluna2 <> CoringaColuna)
   and (Coluna3 <> CoringaColuna) then //Se n�o possui coringa
   begin
      if (Coluna1 < Coluna2)
      and (Coluna1 < Coluna3) then
         Result := 1
      else
      if (Coluna2 < Coluna1)
      and (Coluna2 < Coluna3) then
         Result := 2
      else
      if (Coluna3 < Coluna1)
      and (Coluna3 < Coluna2) then
         Result := 3
      else //As 3 cartas s�o iguais
      begin
         if Mao[0] <> '' then
            Result := 1
         else if Mao[1] <> '' then
            Result := 2
         else if Mao[2] <> '' then
            Result := 3;
      end;
   end else
   begin
      if (Coluna1 = CoringaColuna) //Se todas as cartas sao manilias e ela n�o tem o gato.
      and (Coluna2 = CoringaColuna)
      and (Coluna3 = CoringaColuna) then
      begin
         if (linha1 < linha2)
         and (linha1 < linha3) then
            Result := 1
         else
         if (linha2 < linha1)
         and (linha2 < linha3) then
            Result := 2
         else
         if (linha3 < linha1)
         and (linha3 < linha2) then
            Result := 3;
      end else
      begin
         if (Coluna1 < Coluna2)
         and (Coluna1 < Coluna3)
         or (Coluna1 = CoringaColuna) then
            Result := 1
         else
         if (Coluna2 < Coluna1)
         and (Coluna2 < Coluna3)
         or (Coluna2 = CoringaColuna) then
            Result := 2
         else
         if (Coluna3 < Coluna1)
         and (Coluna3 < Coluna2)
         or (Coluna3 = CoringaColuna) then
            Result := 3
         else //As 3 cartas s�o iguais
         begin
            if Mao[0] <> '' then
               Result := 1
            else if Mao[1] <> '' then
               Result := 2
            else if Mao[2] <> '' then
               Result := 3;
         end;
      end;
   end;
end;

function TfoTruco.EncontraCartaMaisFraca(Vetor : array of String): Integer;
var i,j,
    Linha1, Coluna1, //Carta 1
    Linha2, Coluna2, //Carta 2
    Linha3, Coluna3, //Carta 3
    CoringaColuna : integer;
begin
   Linha1:=0;
   Coluna1:=0;
   Linha2:=0;
   Coluna2:=0;
   Linha3:=0;
   Coluna3:=0;

   for I := 1 to 4 do
      for j := 1 to 10 do
      begin
         if Vetor[0] <> '' then
            if (Baralho[i,j] = Vetor[0]) then
            begin
               Linha1 := i;
               Coluna1 := j;
            end;
         if Vetor[1] <> '' then
            if (Baralho[i,j] = Vetor[1]) then
            begin
               Linha2 := i;
               Coluna2 := j;
            end;
         if Vetor[2] <> '' then
            if (Baralho[i,j] = Vetor[2]) then
            begin
               Linha3 := i;
               Coluna3 := j;
            end;
         if (Baralho[i,j] = Coringa) then
         begin
            CoringaColuna := j-1;
         end;
      end;

   if CoringaColuna = 1 then
      CoringaColuna := 10
   else
      Dec(CoringaColuna);

   if (Coluna1 <> CoringaColuna)
   and (Coluna2 <> CoringaColuna)
   and (Coluna3 <> CoringaColuna) then //Se n�o possui coringa
   begin
      if (Coluna1 > Coluna2)
      and (Coluna1 > Coluna3) then
         Result := 1
      else
      if (Coluna2 > Coluna1)
      and (Coluna2 > Coluna3) then
         Result := 2
      else
      if (Coluna3 > Coluna1)
      and (Coluna3 > Coluna2) then
         Result := 3
      else //As 3 cartas s�o iguais
      begin
         if Vetor[0] <> '' then
            Result := 1
         else if Vetor[1] <> '' then
            Result := 2
         else if Vetor[2] <> '' then
            Result := 3;
      end;
   end else
   begin
      if (Coluna1 = CoringaColuna) //Se todas as cartas sao manilias e ela n�o tem o gato.
      and (Coluna2 = CoringaColuna)
      and (Coluna3 = CoringaColuna) then
      begin
         if (linha1 > linha2)
         and (linha1 > linha3) then
            Result := 1
         else
         if (linha2 > linha1)
         and (linha2 > linha3) then
            Result := 2
         else
         if (linha3 > linha1)
         and (linha3 > linha2) then
            Result := 3;
      end else
      begin
         if (Coluna1 > Coluna2)
         and (Coluna1 > Coluna3)
         and (Coluna1 <> CoringaColuna) then
            Result := 1
         else
         if (Coluna2 > Coluna1)
         and (Coluna2 > Coluna3)
         and (Coluna2 <> CoringaColuna) then
            Result := 2
         else
         if (Coluna3 > Coluna1)
         and (Coluna3 > Coluna2)
         and (Coluna3 <> CoringaColuna) then
            Result := 3
         else //As 3 cartas s�o iguais
         begin
            if Vetor[0] <> '' then
               Result := 1
            else if Vetor[1] <> '' then
               Result := 2
            else if Vetor[2] <> '' then
               Result := 3;
         end;
      end;
   end;
end;

function TfoTruco.EncontraCartaMedia(Mao: array of String) : Integer;
var CartaForte, CartaFraca : integer;
begin
   CartaForte := EncontraCartaForte(Mao);

   CartaFraca := EncontraCartaMaisFraca(Mao);

   if (Mao[1] <> Mao[CartaForte]) and
      (Mao[1] <> Mao[CartaFraca]) then
   begin
      result := 1;
   end else 
   if (Mao[2] <> Mao[CartaForte]) and
      (Mao[2] <> Mao[CartaFraca]) then
   begin
      result := 2;
   end else 
   if (Mao[3] <> Mao[CartaForte]) and
      (Mao[3] <> Mao[CartaFraca]) then 
   begin
      result := 3;
   end;
end;

procedure TfoTruco.FormShow(Sender: TObject);
begin
   DefinirBaralho;
   FgJogoIniciado := false;
   FgModoDesenvolvedor := false;
   BtDesenvolvedor.Visible := False;
   BtDesenvolvedor.Visible := true;
   BtPadraoClick(sender);
end;

procedure TfoTruco.HabilitaDesabilitaTrucar;
begin
   if VezIA.Visible
   and FgTrucoIA then //IA
   begin
      BtTrucoIA.Enabled := True;
      BtTruco.Enabled := False;
   end else //Player
   begin
      BtTrucoIA.Enabled := False;
      BtTruco.Enabled := True;
   end;

   if VezPlayer.Visible
   and FgTrucoPlayer then
   begin
      BtTruco.Enabled := false;
   end;
end;

procedure TfoTruco.IARecebePontos(Pontos: Integer);
begin
   if Pontos >= 3 then
      PlaySound('C:\Efeitos Sonoros\C21.WAV', 1, SND_ASYNC);
   InFezPrimeira := 3;
   Timer1.Enabled := False;
   Rodada := 0;
   CartaJogadaIA.Caption := '';
   SetaImagemCarta(CartaJogadaIA,'');

   CartaJogadaIA.Visible := False;
   CartaJogada.Caption := '';
   SetaImagemCarta(CartaJogada,'');

   CartaJogada.Visible := False;
   if FgComeca = 0 then
   begin
    FgComeca := 1;
    VezPlayer.Visible := True;
    VezIA.Visible := False;
   end else
   begin
    FgComeca := 0;
    VezPlayer.Visible := False;
    VezIA.Visible := True;
   end;
   SpeedButton4.Enabled := false;
   SpeedButton5.Enabled := false;
   SpeedButton1.Enabled := false;
   SpeedButton2.Enabled := false;
   QtRodadasGanhaIA := 0;
   QtRodadasGanhaPlayer := 0;

   edEles.Text := IntToStr((StrToInt(edEles.Text)+Pontos));

   BtTruco.Enabled := True;
   BtTrucoIA.Enabled := True;
   FgTrucoPlayer := False;
   FgTrucoIA := False;
   FgEmpachou1 := false;
   FgEmpachou2 := false;

   if (StrToInt(edEles.Text) >= 12)
   or (StrToInt(edNos.Text) >= 12)then
    ReiniciaJogo;
   HabilitaDesabilitaTrucar;
   IniciaJogo;
end;

procedure TfoTruco.IAVenceRodada;
begin
   if Rodada = 1 then
      InFezPrimeira := 0;

   FgComecaRodada := 0; //IA come�a
   QtRodadasGanhaIA := QtRodadasGanhaIA+1;
   if QtRodadasGanhaIA = 1 then
      SpeedButton4.Enabled := true
   else
   begin
      SpeedButton5.Enabled := true;
   end;
   CartaJogadaIA.Caption := '';
   SetaImagemCarta(CartaJogadaIA,'');
   CartaJogadaIA.Visible:= False;

   VezPlayer.Visible := False;
   VezIA.Visible := True;
   HabilitaDesabilitaTrucar;
end;

procedure TfoTruco.SetaImagemCarta(Sender:TObject;Caption: String);
var i,j:integer;
begin
   //4 imageLists
   if not (FgModoDesenvolvedor) //Se n�o est� em modo desenvolvedor deve esconder as cartas da IA.
   and ((TComponent(sender).Name = 'CartaIA1')
   or (TComponent(sender).Name = 'CartaIA2')
   or (TComponent(sender).Name = 'CartaIA3'))then
   begin
      (Sender as TJvImgBtn).Images := ImagemDefault;
      (Sender as TJvImgBtn).ImageIndex := 0;
   end else //Se n�o ela troca a imagem.
   if (Caption = '') then
   begin
      (Sender as TJvImgBtn).Images := ImagemDefault;
      (Sender as TJvImgBtn).ImageIndex := 0;
   end else
   begin
      for i := 1 to 4 do
         for j := 1 to 10 do
         begin
            case i of
               1: if Caption = Baralho[i,j] then
                  begin
                     (Sender as TJvImgBtn).Images := Paus;
                     (Sender as TJvImgBtn).ImageIndex := j-1;
                  end;
               2: if Caption = Baralho[i,j] then
                  begin
                     (Sender as TJvImgBtn).Images := Copas;
                     (Sender as TJvImgBtn).ImageIndex := j-1;
                  end;
               3: if Caption = Baralho[i,j] then
                  begin
                     (Sender as TJvImgBtn).Images := Espada;
                     (Sender as TJvImgBtn).ImageIndex := j-1;
                  end;
               4: if Caption = Baralho[i,j] then
                  begin
                     (Sender as TJvImgBtn).Images := Ouro;
                     (Sender as TJvImgBtn).ImageIndex := j-1;
                  end;
            end;
         end;
   end;
end;

procedure TfoTruco.IniciaJogo;
begin
   //Seta os pontos de cada embaralhada
   ForcaMaoIA := 0;
   PontosValidosJogo := 1;
   DarCartas;
   VerificaQuemComeca;
   InFezPrimeira := 3;
   //Aguarda um tempo fixo at� a a��o da IA (para n�o jogar instant�neamente)
   Timer1.Enabled := True;
end;

procedure TfoTruco.InicializaImagemCartas;
begin
   SetaImagemCarta(Carta1,'');
   SetaImagemCarta(Carta2,'');
   SetaImagemCarta(Carta3,'');
   SetaImagemCarta(CartaIA1,'');
   SetaImagemCarta(CartaIA2,'');
   SetaImagemCarta(CartaIA3,'');
   SetaImagemCarta(CartaVirada,'');
   SetaImagemCarta(CartaJogadaIA,'');
   SetaImagemCarta(CartaJogada,'');
end;

procedure TfoTruco.LimpaPesoCartasIA;
begin
   PesoCartasIA[1,1] := 0;
   PesoCartasIA[1,2] := 0;

   PesoCartasIA[2,1] := 0;
   PesoCartasIA[2,2] := 0;

   PesoCartasIA[3,1] := 0;
   PesoCartasIA[3,2] := 0;
end;

function TfoTruco.PerguntaSN(const Msg: String; DefButton: Word): Word;
Var I : LongInt;
begin
   with CreateMessageDialog(Msg, mtconfirmation, [mbYes,mbNo]) do
        try
           Caption := 'Confirma��o';
           HelpContext := 0;
           For i := 0 to ComponentCount - 1 do
           begin
              if  (Components[i] is TButton) then
              begin
                 case TButton(Components[i]).ModalResult of
                      mrYes     : TButton(Components[i]).Caption := 'Sim';
                      mrCancel  : TButton(Components[i]).Caption := 'Cancelar';
                      mrAbort   : TButton(Components[i]).Caption := 'Abortar';
                      mrIgnore  : TButton(Components[i]).Caption := 'Ignorar';
                      mrNo      : TButton(Components[i]).Caption := 'N�o';
                      mrOk      : TButton(Components[i]).Caption := 'OK';
                      mrRetry   : TButton(Components[i]).Caption := 'Tentar de novo';
                      mrAll     : TButton(Components[i]).Caption := 'Tudo';
                 end;
                 if (TButton(Components[i]).ModalResult = DefButton) then
                    ActiveControl := TButton(Components[i]);
              end;
           end;
           Result := ShowModal;
        finally
           free;
        end;
end;

procedure TfoTruco.PlayerRecebePontos(Pontos: Integer);
begin
    Timer1.Enabled := False;
    Rodada := 0;
    CartaJogadaIA.Caption := '';
    SetaImagemCarta(CartaJogadaIA,'');
    CartaJogadaIA.Visible := False;
    InFezPrimeira := 3;
    CartaJogada.Caption := '';
    SetaImagemCarta(CartaJogada,'');
    CartaJogada.Visible := False;
    if FgComeca = 0 then
    begin
       FgComeca := 1;
       VezPlayer.Visible := True;
       VezIA.Visible := False;
    end else
    begin
       FgComeca := 0;
       VezPlayer.Visible := False;
       VezIA.Visible := True;
    end;
    SpeedButton4.Enabled := false;
    SpeedButton5.Enabled := false;
    SpeedButton1.Enabled := false;
    SpeedButton2.Enabled := false;
    QtRodadasGanhaIA := 0;
    QtRodadasGanhaPlayer := 0;

    edNos.Text := IntToStr((StrToInt(edNos.Text)+Pontos));

    BtTruco.Enabled := True;
    BtTrucoIA.Enabled := True;
    FgTrucoPlayer := False;
    FgTrucoIA := False;
    FgEmpachou1 := false;
    FgEmpachou2 := false;

    if (StrToInt(edEles.Text) >= 12)
    or (StrToInt(edNos.Text) >= 12)then
       ReiniciaJogo;
    HabilitaDesabilitaTrucar;
    IniciaJogo;
end;

procedure TfoTruco.PlayerVenceRodada;
begin
   if Rodada = 1 then
      InFezPrimeira := 1;

   FgComecaRodada := 1; //Player come�a
   QtRodadasGanhaPlayer := QtRodadasGanhaPlayer+1;
   if QtRodadasGanhaPlayer = 1 then
      SpeedButton1.Enabled := true
   else
   begin
      SpeedButton2.Enabled := true;
   end;
   CartaJogada.Caption := '';
   SetaImagemCarta(CartaJogada,'');
   CartaJogada.Visible := False;
end;

function TfoTruco.ProcuraCartaNaMaoMaisForteIA(CartaPlayer: String = ''): Integer;
var i,j,l,k,
    CartaIAlinha,
    CartaIAColuna,
    CartaPLinha,
    CartaPColuna,
    CoringaColuna : integer;
    CartaMaisForte,
    CartaEmpate : Integer;
    JogaCartaIA : Array[1..3] of String;
    JogaCartaIAOld,
    JogaCartaMaisFraca : Integer;
//    AvaliaMao : Boolean;
begin
   L := 0;
   CartaMaisForte := 0;
   CartaEmpate := 0;
   CoringaColuna := 0;
   CartaIAlinha := 0;
   CartaIAColuna := 0;
   CartaPLinha := 0;
   CartaPColuna := 0;
//   AvaliaMao := (CartaPlayer = '');

   while L < 3 do //3 Verifica��o de quais cartas da IA que matam ou empatam com a Carta do Jogada
   begin         // Onde elas seram jogadas em um novo vetor para compara��o entre elas
      Inc(L);
      if (CartaJogadaIA.Caption = '') then
      begin
//         if AvaliaMao
//         and ((Coringa <> '3-P')
//         and (Coringa <> '3-C')
//         and (Coringa <> '3-E')
//         and (Coringa <> '3-O')) then
//            CartaPlayer := '4-O'
//         else
//            CartaPlayer := '5-O';

         for I := 1 to 4 do
            for j := 1 to 10 do
            begin
               if (Baralho[i,j] = IA[L]) then
               begin
                  CartaIAlinha := i;
                  CartaIAColuna := j;
               end;
               if (Baralho[i,j] = CartaPlayer) then
               begin
                  CartaPLinha := i;
                  CartaPColuna := j;
               end;
               if (Baralho[i,j] = Coringa) then
               begin
                  CoringaColuna := j-1;
               end;
            end;
      end;

   if CoringaColuna = 1 then
      CoringaColuna := 10
   else
      Dec(CoringaColuna);

      if IA[L] <> '' then
      begin
         if ((CartaPLinha = CartaIAlinha)
         and (CartaPColuna <> CoringaColuna)
         and (CartaIAColuna <> CoringaColuna)) then  //Sao de mesmo nipe mas sem nenhum manilia
         begin
            if CartaPColuna > CartaIAColuna then
            begin
               JogaCartaIA[L] := IA[L];
               //Break; //Resulta com o indice do vetor que contem a carta que � mais forte que a jogada pelo player
            end;
         end else //Nipes diferentes
         if (CartaPLinha <> CartaIAlinha) then
         begin
            if (CartaPColuna = CoringaColuna)
            and (CartaIAColuna = CoringaColuna) then //Disputa de manilia mais forte
            begin
               if CartaPLinha > CartaIALinha then //menor linha igual a naipe mais forte
               begin
                  JogaCartaIA[L] := IA[L]; //Resulta como o indice do vetor que contem a carta que � mais forte que a jogada pelo player
               end;
            end else
            if (CartaPColuna <> CoringaColuna)
            and (CartaIAColuna = CoringaColuna) then
            begin
               JogaCartaIA[L] := IA[L]; //Resulta como o indice do vetor que contem a carta que � mais forte que a jogada pelo player;
            end else
            if (CartaPColuna <> CoringaColuna)
            and (CartaIAColuna <> CoringaColuna) then
            begin
               if CartaPColuna = CartaIAColuna then //Cartas Iguais empatam e ninguem ganha ponto de rodada
               begin
                  if JogaCartaIA[L] = '' then
                  begin
                     CartaEmpate := L;
                  end;    //Empate deve procurar proxima carta porem ainda nao foi emplementado
               end else
               if CartaPColuna > CartaIAColuna then
               begin
                  JogaCartaIA[L] := IA[L]; //Resulta como o indice do vetor que contem a carta que � mais forte que a jogada pelo player
               end;
            end;// else
         end else
         begin
            if (CartaIAColuna = CoringaColuna) then //IA tem manilia e Player nao tem
            begin
               JogaCartaIA[L] := IA[L]; //Resulta como o indice do vetor que contem a carta que � mais forte que a jogada pelo player
            end;
         end;
      end;
   end;
   
//   if not AvaliaMao then
   CartaMaisForte := EncontraCartaMaisFraca(JogaCartaIA);

   if (CartaMaisForte = 0)
   and (CartaEmpate > 0) then
      CartaMaisForte := CartaEmpate
   else
   if (CartaMaisForte = 0)
   and (CartaEmpate = 0)then
      CartaMaisForte := EncontraCartaMaisFraca(IA);



   Result := CartaMaisForte;
end;

procedure TfoTruco.ReiniciaJogo;
var a,b : integer;
begin
   FgJogoIniciado := False;

   if (StrToInt(edNos.Text) >= 12) then
      ShowMessage('Jogador ONO o IA noob! GG!');
   if (StrToInt(edEles.Text) >= 12) then
      ShowMessage('IA venceu e disse easy mid easy life!!! THUG LIFE!');

   for a := 1 to 7 do
      Jogo[a] := '';

   for b := 1 to 3 do
   begin
      IA[b] := '';
      Player1[b] := '';
   end;

   CartaJogadaIA.Caption := '';
   SetaImagemCarta(CartaJogadaIA,'');

   QtRodadasGanhaPlayer:= 0;
   QtRodadasGanhaIA := 0;

   edNos.Text := '0';
   edEles.Text := '0';
end;

procedure TfoTruco.Timer1Timer(Sender: TObject);
var CartaIA : Integer;
begin
   HabilitaDesabilitaTrucar;
   if ((CartaJogada.Caption <> '')
   and (CartaJogadaIA.Caption <> ''))
   and ((StrToInt(edNos.Text) < 12)
   and (StrToInt(edEles.Text) < 12))
   and (Rodada <= 3) then
   begin
      ValidaJogada;
      //Pensar no caso de empachar as 3 rodadas*
      //Define quem ganhar� os pontos da rodada(quem ganhou a rodada?)
      if (bEmpachou)
      and (QtRodadasGanhaPlayer <> QtRodadasGanhaIA) then
      begin
         bEmpachou := False;
         if QtRodadasGanhaPlayer > QtRodadasGanhaIA then
            PlayerRecebePontos(PontosValidosJogo)
         else
            IARecebePontos(PontosValidosJogo);

         {vari�vel booleana para difinir quem ganhou a primeira rodada,
         para ser utilizada quando � feito a primeira, perdido a segunda e empachado a
         terceira}
      end
      else if (rodada = 3)
      and fgEmpachou1
      and fgEmpachou2
      and (bEmpachou) {Variavel de empache da rodada atual}then
      begin
         PlayerRecebePontos(0); //Empachou tudo 0.000001% de acontecer
      end
      else
      if (Rodada = 3)
      and (bEmpachou) then
      begin
         if InFezPrimeira = 0 then
            IARecebePontos(PontosValidosJogo)
         else
            PlayerRecebePontos(PontosValidosJogo);
      end else
      if (Rodada <= 3)
           and not ((QtRodadasGanhaPlayer = 2)
                and (QtRodadasGanhaIA = 2))
           and ((QtRodadasGanhaPlayer = 2)
                or (QtRodadasGanhaIA = 2)) then
      begin
         if FgComecaRodada = 1 then //Player ganhou 1 ponto
         begin
            PlayerRecebePontos(PontosValidosJogo)
         end else
            IARecebePontos(PontosValidosJogo);
      end else
      begin
         CartaJogadaIA.Caption := '';
         SetaImagemCarta(CartaJogadaIA,'');
         CartaJogadaIA.Visible := False;

         CartaJogada.Caption := '';
         SetaImagemCarta(CartaJogada,'');
         CartaJogada.Visible := False;
         VezIA.Visible := False;
         VezPlayer.Visible := True;
         HabilitaDesabilitaTrucar;
      end;
   end else
   if (CartaJogadaIA.Caption = '')
   and ((CartaJogada.Caption <> '')
   or  (FgComecaRodada = 0))
   and ((StrToInt(edNos.Text) < 12)
   and (StrToInt(edEles.Text) < 12))
   and (Rodada <= 3)
   //and (Rodada > 0))
   then
   begin
      if (CartaJogada.Caption <> '') then
      begin
         if FgRetrucoRodada //IA pode retrucar
         and FgTrucoPlayer then
         begin
            btTrucoIAClick(Sender);
         end else
         if not(FgTrucoIA) //Se IA n�o trucou ou retrucou
         and (FgTrucoRodada //Facao
           or FgFacao)then
            btTrucoIAClick(Sender);

         CartaIA := ProcuraCartaNaMaoMaisForteIA(CartaJogada.Caption);
         while IA[CartaIA] = '' do
            CartaIA := ProcuraCartaNaMaoMaisForteIA(CartaJogada.Caption);
      end else
      begin
         if (Rodada in [1,2])
         and (SpeedButton4.Enabled) then
            btTrucoIAClick(Sender)
         else
            if FgRetrucoRodada //IA pode retrucar
            and FgTrucoPlayer then
            begin
               btTrucoIAClick(Sender);
            end else
            if not(FgTrucoIA) //Se IA n�o trucou ou retrucou
               and (FgTrucoRodada //Facao
              or FgFacao)then
               btTrucoIAClick(Sender);

         CartaIA := EncontraCartaForte(IA);

         //Metodo Antigo de retorno randomico
//         CartaIA := RandomRange(1,4);
//         while IA[CartaIA] = '' do
//            CartaIA := RandomRange(1,4);
      end;

      TiraCartaMao(CartaIA);
   end;
end;

procedure TfoTruco.TiraCartaMao(CartaIA: Integer);
begin
   case CartaIA of
      1 : CartaIA1.Visible := False;
      2 : CartaIA2.Visible := False;
      3 : CartaIA3.Visible := False;
   end;

   CartaJogadaIA.Caption := IA[CartaIA];
   SetaImagemCarta(CartaJogadaIA,IA[CartaIA]);

   IA[CartaIA] := '';
   CartaJogadaIA.Visible := Enabled;
   VezPlayer.Visible := True;
   VezIA.Visible := False;
   HabilitaDesabilitaTrucar;
end;

//Aplica as regras do jogo
//Verifica com as cartas jogadas quem foi o vencedor do turno ou rodada
procedure TfoTruco.ValidaJogada;
var i,j, CartaIAlinha, CartaIAColuna, CartaPLinha, CartaPColuna,
    CoringaColuna : integer;
begin
   //Descobre a for�a(n�mero) da carta e o naipe para compara��o de for�as
   if (CartaJogadaIA.Caption <> '')
   and (CartaJogada.Caption <> '') then
   begin
      Inc(Rodada);
      for I := 1 to 4 do
         for j := 1 to 10 do
         begin
            if (Baralho[i,j] = CartaJogadaIA.Caption) then
            begin
               CartaIAlinha := i;
               CartaIAColuna := j;
            end;
            if (Baralho[i,j] = CartaJogada.Caption) then
            begin
               CartaPLinha := i;
               CartaPColuna := j;
            end;
            if (Baralho[i,j] = Coringa) then
            begin
               CoringaColuna := j;
            end;
         end;
   end;

   if CoringaColuna = 1 then
      CoringaColuna := 10
   else
      Dec(CoringaColuna);

   if ((CartaPLinha = CartaIAlinha)
   and (CartaPColuna <> CoringaColuna)
   and (CartaIAColuna <> CoringaColuna)) then  //Sao de mesmo nipe mas sem nenhuma manilha
   begin
      if CartaPColuna < CartaIAColuna then
      begin
         PlayerVenceRodada;
      end else if CartaPColuna > CartaIAColuna then
      begin
         IAVenceRodada;
      end;
   end else //Nipes diferentes
   if (CartaPLinha <> CartaIAlinha) then
   begin
      if (CartaPColuna = CoringaColuna)
      and (CartaIAColuna = CoringaColuna) then //Disputa de manilia mais forte
      begin
         if CartaPLinha < CartaIALinha then //menor linha igual a naipe mais forte
         begin
            //Player ganha
            PlayerVenceRodada;
         end else
         begin
            PlaySound('C:\Efeitos Sonoros\E35.WAV', 1, SND_ASYNC);
            Sleep(4000);
            //IA ganha
            IAVenceRodada;
         end;
      end else
      if (CartaPColuna = CoringaColuna)
      and (CartaIAColuna <> CoringaColuna) then
      begin
         PlayerVenceRodada;
      end else
      if (CartaPColuna <> CoringaColuna)
      and (CartaIAColuna = CoringaColuna) then
      begin
         PlaySound('C:\Efeitos Sonoros\E35.WAV', 1, SND_ASYNC);
         Sleep(4000);
         IAVenceRodada;
      end else
      if (CartaPColuna <> CoringaColuna)
      and (CartaIAColuna <> CoringaColuna) then //N�o s�o coringas
      begin
         if CartaPColuna = CartaIAColuna then //Cartas Iguais empatam e ninguem ganha ponto de rodada
         begin
            if rodada = 1 then
               FgEmpachou1 := true;

            if rodada = 2 then
               FgEmpachou2 := true;

            if FgComecarodada = 0 then
               FgComecaRodada := 1
            else
               FgComecaRodada := 0;

            //if Rodada = 2 then
            bEmpachou := True;
            PlayerVenceRodada;
            IAVenceRodada;
         end else
         if CartaPColuna < CartaIAColuna then
         begin
            PlayerVenceRodada
         end else
            IAVenceRodada;
      end;
   end else
   begin
      if (CartaIAColuna = CoringaColuna) then //IA tem manilia e Player nao tem
      begin
         IAVenceRodada;
      end;
      if (CartaPColuna = CoringaColuna) then //Player tem manilia e IA nao tem
      begin
         PlayerVenceRodada; //Resulta como o indice do vetor que contem a carta que � mais forte que a jogada pelo player
      end;
   end;

end;

//Valida se a carta randomizada j� n�o est� no jogo
function TfoTruco.validaJogo(Carta : String): Boolean;
begin
    result := True;
    if not ((Carta = Jogo[1]) or
            (Carta = Jogo[2]) or
            (Carta = Jogo[3]) or
            (Carta = Jogo[4]) or
            (Carta = Jogo[5]) or
            (Carta = Jogo[6]) or
            (Carta = Jogo[7])) then
    begin
       result := False
    end;
end;



procedure TfoTruco.VerificaQuemComeca;
var CartaIA : Integer;
begin

   if (edEles.Text = '0')
   and (edNos.Text = '0') then
   begin
      //EXECUTADO UMA VEZ POR PARTIDA, ap�s isto ser� definido quem come�a
      //nos procedimentos PlayerRecebePontos e IARecebePontos

      //Caso in�cio do round (os 2 jogadores com 0 pontos);
      FgComeca := Randomrange(0,2); //Esta variavel � para o come�o dos jogos ap�s chegar a 12 pontos
      FgComecaRodada := FgComeca; //Esta variavel � para decidir quem come�a as rodadas antes de chegar a 12 pontos
   end;

   ForcaMaoIA := ClassificaMaoIA(IA); //Classifica a For�a da M�o
   if ForcaMaoIA >= 10 then //Caso tenha 2 cartas maiores que 2 ou 3 ou manilha
      FgAceitaTrucoIA := True;


   if (FgComeca = 0)
   or (FgComecaRodada = 0) then
   begin
      //IA COME�A
      //Se for maior que 10 � forte
      //AQUI ENTRA A INTELIG�NCIA ARTIFICIAL (ESTRAT�GIA DA IA)

      {
        * Avaliar a for�a da m�o, escolher estrat�gia de primeira carta
      }

      if ForcaMaoIA >= 10 then
      begin
         FgAceitaTrucoIA := True;

         CartaIA := EncontraCartaMedia(IA);
      end else
      begin
         CartaIA := EncontraCartaForte(IA);
      end;
      TiraCartaMao(CartaIA);
   end else
   begin
      //Player deve jogar
      VezIA.Visible := False;
      VezPlayer.Visible := True;
   end;
   HabilitaDesabilitaTrucar;
end;

end.
