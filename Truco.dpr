program Truco;

uses
  Forms,
  uTruco in 'uTruco.pas' {foTruco},
  uAbout in 'uAbout.pas' {foAbout};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Truco - ECP391';
  Application.CreateForm(TfoTruco, foTruco);
  Application.CreateForm(TfoAbout, foAbout);
  Application.Run;
end.
